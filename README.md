# README #

### Project info

API gateway responsible for access to patient related microservices

Uses:
- Java 11
- Gradle 6.6.1 (use the gradlew wrapper)
- Spring Boot 2.3.4
- Spring Cloud Hoxton.SR8
- Lombok (https://projectlombok.org/)

### Developing

Commandline:
- gradlew build -> Builds and tests the application
- gradlew bootBuildImage -> Generates a Docker image
- gradlew bootRun -> Runs the application